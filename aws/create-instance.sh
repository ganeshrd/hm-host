#!/usr/bin/env bash

########## output colors

NC="\033[0m"
BLUE="\033[0;34m"
GREEN="\033[0;32m"
RED="\033[0;31m"
YELLOW="\033[1;33m"
PURPLE="\033[1;35m"
ORANGE="\033[1;31m"

########## internal functions

control_flow () {
  RET=$1
  if [ ${RET} -eq 255 ]; then
    echo -e "${YELLOW} $2 command validated.${NC}"
  elif [ ${RET} -ne 0 ]; then
    echo -e "${ORANGE} $2 command failed.${NC}"
    exit 1
  else
    echo -e "${GREEN} $2 ${BLUE}${SG_NAME}${GREEN} $3 ${NC}"
  fi
}

# Setup an aws instance to host an housematch system

### CONSTANTS ###

read -p "Enter a type of instance: " -a INSTANCE_TYPE
read -p "Disable dry run? (no-dry-run/...) : " -a CHOICE
if [ ${CHOICE} == "no-dry-run" ]; then
  echo -e "${YELLOW} setup in ${ORANGE}no dry run${YELLOW} mode. ${NC}"
  DRY_RUN="--no-dry-run"
else 
  echo -e "${YELLOW} setup in ${BLUE}dry run${YELLOW} mode. ${NC}"
  DRY_RUN="--dry-run"
  FAKE_ID=0fcde7
fi

IP_BASE=192.168.0
IP=${IP_BASE}.0
SSH_PORT=22
HTTP_PORT=8001

#################

# VPC
echo -ne "${PURPLE}Creating a ${BLUE}virtual private cloud ${NC} ...... "
VpcId=`aws ec2 create-vpc ${DRY_RUN} --cli-input-json file://configs/hm-vpc.conf \
                                     --cidr-block ${IP}/24 \
                                     --query 'Vpc.VpcId' \
                                     --output text`
echo -e "${BLUE} Enter the hm-vpc's name: ${NC}"
read VPC_NAME
aws ec2 create-tags --resources ${VpcId} --tags Key=Name,Value=${VPC_NAME} 
control_flow $? "vpc" "just created"

# ENBALING DNS SUPPORT (needed for erlang node name resolution)
aws ec2 modify-vpc-attribute --enable-dns-support '{"Value" : true }' \
                             --vpc-id ${VpcId}
aws ec2 modify-vpc-attribute --enable-dns-hostnames '{"Value" : true }' \
                             --vpc-id ${VpcId}

# Subnet
echo -ne "${PURPLE}Creating a ${BLUE}subnet ${NC} ...... "
if [ ${DRY_RUN} == --dry-run ]; then
  VpcId=vpc-${FAKE_ID}
fi
echo -e "${BLUE} Choisissze une zone de disponibilité : ${NC}"
select REGION in eu-west-3a eu-west-3b eu-west-3c;
do
  case $REGION in
    eu-west-3a) ZONE=eu-west-3a
                break;;
    eu-west-3b) ZONE=eu-west-3b
                break;;
    eu-west-3c) ZONE=eu-west-3c
                break;;
             *) ZONE=eu-west-3a;;
  esac
done;

SubnetId=`aws ec2 create-subnet ${DRY_RUN} --availability-zone ${ZONE} \
                                           --vpc-id ${VpcId} \
                                           --cidr-block ${IP}/28 \
                                           --query 'Subnet.SubnetId' \
                                           --output text`
control_flow $? "subnet" "created"

# Internet gateway
echo -ne "${PURPLE}Attaching internet gateway to vpc ${NC}...... "
InternetGatewayId=`aws ec2 create-internet-gateway ${DRY_RUN} --query  'InternetGateway.InternetGatewayId' \
                                                              --output text`
aws ec2 attach-internet-gateway ${DRY_RUN} --vpc-id ${VpcId} \
                                           --internet-gateway-id ${InternetGatewayId}
control_flow $? "internet gateway" "attached"

# Route table
echo -ne "${PURPLE}Creating a ${BLUE}route table ${NC} ...... "
RouteTableId=`aws ec2 create-route-table ${DRY_RUN} --vpc-id ${VpcId} \
                                                    --query 'RouteTable.RouteTableId' \
                                                    --output text`
aws ec2 create-route ${DRY_RUN} --destination-cidr-block 0.0.0.0/0 \
                                --gateway-id ${InternetGatewayId} \
                                --route-table-id ${RouteTableId} 1>/dev/null
control_flow $? "route table" "added"

# Route table association
echo -ne "${PURPLE}Associate route table with subnet ${NC}...... "
aws ec2 associate-route-table ${DRY_RUN} --route-table-id ${RouteTableId} --subnet-id ${SubnetId} 1>/dev/null
control_flow $? "route table" "associated"

# Security group
echo -ne "${PURPLE}Creating a ${BLUE}security group ${NC}...... "
SG_NAME="hm-sg"
GroupId=`aws ec2 create-security-group ${DRY_RUN} --cli-input-json file://configs/hm-security-group.conf \
                                                  --vpc-id ${VpcId} \
                                                  --query 'GroupId' \
                                                  --output text`
if [ ${DRY_RUN} == --dry-run ]; then
  GroupId=sg-${FAKE_ID}
fi
# SSH setting
aws ec2 authorize-security-group-ingress ${DRY_RUN} --group-id ${GroupId} \
                                                    --cidr 0.0.0.0/0 \
                                                    --protocol tcp \
                                                    --port ${SSH_PORT}
# HTTP for client's incomes
aws ec2 authorize-security-group-ingress ${DRY_RUN} --group-id ${GroupId} \
                                                    --cidr 0.0.0.0/0 \
                                                    --protocol tcp \
                                                    --port ${HTTP_PORT}
# ICMP for client's incomes jut ping
aws ec2 authorize-security-group-ingress ${DRY_RUN} --group-id ${GroupId} \
                                                    --ip-permissions "IpProtocol=icmp,FromPort=8,ToPort=0,IpRanges=[{CidrIp=0.0.0.0/0}]"
aws ec2 create-tags --resources ${GroupId} --tags Key=Name,Value=${SG_NAME}
control_flow $? "security group" "just created"

# Launch instance
echo -ne "${PURPLE}Attempting to launch an instance of type ${INSTANCE_TYPE} now ${NC} ..... "
if [ ${DRY_RUN} == --dry-run ]; then
  SubnetId=subnet-${FAKE_ID}
fi
out=`aws ec2 run-instances ${DRY_RUN} --cli-input-json file://configs/hm-instance.conf \
                                      --instance-type ${INSTANCE_TYPE} --security-group-ids ${GroupId} \
                                      --subnet-id ${SubnetId} \
                                      --query 'Instances[0].[InstanceId,PublicIpAddress]' \
                                      --output text`
echo -e "${GREEN}instance ${BLUE} ${out} ${GREEN} is starting now ${NC}..."
exit 0
