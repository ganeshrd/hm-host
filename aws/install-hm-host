#!/usr/bin/env bash

# THIS PROGRAM IS UNDER GANESHRD LICENCE
# and is part of the hm_backend 2 tool chain. 
# It install and configure the deployement environement
# for hm_backend 2.
#
# N.B. The script shall be invoked with the source command prepended
# i.e. '. ./install-hm-host.sh' (note the leading dot).
#
# author: Renaud Tripathi <rkdt.ganeshrd@gmail.com>

# file: bash_colors.part
# utility for colored output

NC=0
BLUE=1
GREEN=2
RED=3
PURPLE=4
YELLOW=5
ORANGE=6
MAGENTA=7 
CYAN=8

COLORS=([$NC]="\033[0m" [$BLUE]="\033[0;34m" [$GREEN]="\033[0;32m" [$RED]="\033[0;31m" \ 
        [$PURPLE]="\033[1;35m" [$YELLOW]="\033[1;33m" [$ORANGE]="\033[1;31m" [$MAGENTA]="\033[0;35m" [$CYAN]="\033[0;36m")

pretty_out () {
 IDX=$(echo $1 | tr a-z A-Z)
 shift
 echo "${COLORS[${IDX}]}${@}${COLORS[${NC}]}"
}
pretty () {
  local out=$(pretty_out $@)
  echo $out
}

if [ -z "$PS1" ]; then
  echo -e "$(pretty orange you must launch the script interactivally) $(pretty cyan e.g.) '. ./install-hm-host'"
  exit 1
fi

apt_install () {
  sudo apt-get -qq install $1 || echo -e "$(pretty red $1 install failed. abort)"
}

###################@

# internal constants

ERLANG_CONFIG_OPTS="--with-ssl=/usr/lib/x86_64-linux-gnu --with-ssl-incl=/usr/include/openssl"
ERLANG_COMPILE_OPTS=
EXPORTS=${HOME}/bin
CWD=`pwd`
TMP_FILE=tmp_file
BITBUCKET_TEAM=ganeshrd
BITBUCKET_USER=rkdt
BITBUCKET_PWD=NASto2728
BITBUCKET_SSH_REST_API=https://api.bitbucket.org/2.0/users/${BITBUCKET_USER}/ssh-keys
REBAR_VSN=3.6.2

if [ ! -d ${EXPORTS} ]; then
  mkdir -p ${EXPORTS}
fi

########################
# SYSTEM SETUP SSH KEY
########################
SSH_KEY_NAME=auto_key
if [ ! -e ${HOME}/.ssh/${SSH_KEY_NAME} ]; then
  ssh-keygen -t rsa -C "`whoami`@`hostname`" -f ${HOME}/.ssh/${SSH_KEY_NAME} -P housematch
fi

eval `ssh-agent -s`
ssh-add ${HOME}/.ssh/${SSH_KEY_NAME}

###############################
# BITBUCKET CREDENTIALS SETUP
###############################
echo -e "{\"key\":\"`cat ${HOME}/.ssh/${SSH_KEY_NAME}.pub`\"}" > ${TMP_FILE}
curl -s -u ${BITBUCKET_USER}:${BITBUCKET_PWD} -X POST -H "Content-Type: application/json" -d @${TMP_FILE} ${BITBUCKET_SSH_REST_API} >/dev/null

############################
# SYSTEM SETUP USER PROFILE
############################
PROFILE=.bash-`whoami`
if [ ! -e ${HOME}/${PROFILE} ];then
  touch ${HOME}/${PROFILE}
  # make it effective
  echo "" >> ${HOME}/.bashrc
  echo -e "# This section has been generated automatically by install-hm-host script" >> ${HOME}/.bashrc
  echo -e "if [ -f ~/${PROFILE} ]; then \n . ~/${PROFILE} \n fi" >> ${HOME}/.bashrc
fi

############################
# SYSTEM SETUP APT INSTALL
############################
sudo apt-get -qq update

# packages dependencies
for package in gcc make m4 libssl1.0-dev libncurses5-dev unzip;
do
  echo -ne "$(pretty purple Installing package) $(pretty blue ${package}) ... "
  apt_install ${package}
  echo -e "$(pretty green done with) $(pretty blue ${package}) $(pretty purple install)"
done

######################
# ERLANG R19 INSTALL
######################
if [ -z `which erl` ]; then
  ERLANG="otp_src_19.3"

  if [ ! -e ${ERLANG} ]; then
    echo -e "$(pretty purple Download) $(pretty blue erlang R19) ... "
    curl --trace-time -O http://erlang.org/download/${ERLANG}.tar.gz
    echo -e "$(pretty green done downloading) $(pretty blue erlang R19)"
    tar xf ${ERLANG}.tar.gz
  fi

  cd ${ERLANG}
  export ERL_TOP=`pwd`
  
  # configuration
  echo -e "$(pretty yellow ready to configure) $(pretty blue erlang 19)"
  echo -e "$(pretty purple configuration status is :) $(pretty blue ${ERLANG_CONFIG_OPTS})"
  echo -e "$(pretty cyan you can) 'stop' $(pretty cyan hm-host install process or add options)"
  echo -e "$(pretty cyan just press 'enter' to proceed without additional options)"
  echo -e "$(pretty yellow configure? : )"
  read CONFIG_OPTS
  if [ ${CONFIG_OPTS} == "stop" ]; then
    exit 1
  fi
  OPTIONS="${ERLANG_CONFIG_OPTS} ${CONFIG_OPTS}"
  echo -e "$(pretty purple starting configuration with options) $(pretty blue ${OPTIONS})"
  ./configure -q ${OPTIONS}
  echo -e "$(pretty green done with erlang 19 configuration.)"

  # compilation
  echo -e "$(pretty yellow ready to compile) $(pretty blue erlang 19)"
  echo -e "$(pretty purple compilation status is :) $(pretty blue ${ERLANG_COMPILE_OPTS})"
  echo -e "$(pretty cyan you can) 'stop' $(pretty cyan hm-host install process or add options e.g.) '-s' $(pretty cyan for quiet mode)"
  echo -e "$(pretty cyan just press) 'enter' $(pretty cyan to proceed without additional options)"
  echo -e "$(pretty yellow compile? : )"
  read COMPILE_OPTS
  if [ ${COMPILE_OPTS} == "stop" ]; then
    exit 1
  fi
  echo -e "$(pretty purple compiling with options) $(pretty blue ${COMPILE_OPTS})"
  make ${COMPILE_OPTS}
  echo -e "$(pretty green done with erlang 19 compilation.)"

  # installation
  echo -e "$(pretty yellow ready to install) $(pretty blue erlang 19)"
  echo -e "$(pretty cyan you can) 'stop' $(pretty cyan hm-host install process or press) enter $(pretty cyan to 'continue')"
  echo -e "$(pretty yellow install? : )"
  read INSTALL_CMD
  if [ ${INSTALL_CMD} == "stop" ]; then
    exit 1
  fi
  echo -e "$(pretty purple installing) $(pretty blue erlang 19) ...)"
  sudo make install
  echo -e "$(pretty green done with erlang 19 install.)"

  cd ${CWD}
fi


#################
# REBAR3 INSTALL
#################
if [ ! -e "${HOME}/bin/rebar3" ]; then
  echo -ne "$(pretty purple installing rebar3) ... "
  git clone https://github.com/erlang/rebar3.git
  cd rebar3
  git checkout tags/${REBAR_VSN}
  ./bootstrap
  mv rebar3 ${HOME}/bin
  echo -e "$(pretty green done with) $(pretty blue rebar3) $(pretty purple install)"
  cd ${CWD}
fi

######################
# AWS CLI INSTALL 
######################
if [ -z `which pip3` ]; then
  echo -e "$(pretty purple installing) $(pretty blue AWS CLI) ... "
  apt_install python3-pip
  pip3 install awscli --upgrade --user
  EXPORTS=$EXPORTS:~/.local/bin
  echo -e "$(pretty green done with) $(pretty blue aws cli) $(pretty purple install)"
fi

########################
# CLONE HM-HOST PROJECT
########################
if [ ! -d hm-host ]; then
  echo -ne "$(pretty purple cloning) $(pretty blue hm-host)  ... "
  git clone ${BITBUCKET_USER}@bitbucket.org:${BITBUCKET_TEAM}/hm-host.git
  cd hm-host
  git submodule update --init
  cd ${CWD}
  echo -e "$(pretty green done with) $(pretty blue hm-host) $(pretty purple intialization)"
fi

echo -e "$(pretty purple configuring) $(pretty blue hm-host environement) ... "
cd hm-host/confihm
chmod +x regen.sh && ./regen.sh
CONFIHM_DIR=`pwd`
EXPORTS=${EXPORTS}:${CONFIHM_DIR}/exports
cd ${CWD}
echo -e "$(pretty green done with) $(pretty blue hm-host environement) $(pretty purple configuration)"

######################
# ENV VARIABLES SETUP
######################
echo -e "$(pretty purple setting up environement variables) $(pretty blue ${EXPORTS}) ... "
if [[ -e ${HOME}/${PROFILE} && ! -s ${HOME}/${PROFILE} ]]; then
  echo -e "export CONFIHM_DIR=${CONFIHM_DIR}" >> ${HOME}/${PROFILE}
  echo -e "export PATH=${EXPORTS}:\$PATH" >> ${HOME}/${PROFILE}
fi
source ~/.bashrc
echo -e "$(pretty green done with) $(pretty blue environement variables) $(pretty purple export)"

##############
# HM-HOST INIT
##############
echo ""
echo -e "$(pretty blue ==============================================)"
echo -e "$(pretty blue ==============) $(pretty orange HOUSEMATCH SETUP) $(pretty blue ==============)"
echo -e "$(pretty blue ==============================================)"
echo ""
echo -ne "$(pretty blue hm-host) $(pretty yellow deployement ... )"
cd hm-host
confihm-init -f confihm.conf
chmod +x setup.sh
./setup.sh
./hm install

###############
# CLEANNING UP 
###############
cd ${CWD}
rm ${TMP_FILE}
rm ${ERLANG}.tar.gz
echo -e "$(pretty cyan you can now proceed with '(cd hm-host and ./boot_nodes)')"
