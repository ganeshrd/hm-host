# hm-host

hmb2 unit.

## prerequisites
__AWS_CLI__ installed and configure at admin level i.e. you have a valid access key and proper authorizations.

## usage

### host instantiation
```
$ cd hm-host/aws
$ ./create_instance.sh
```
The script ask for an __AWS__ instance type _e.g. t2.micro_ and whether or not it should be run in simulation mode.  
__`create_instance.sh` crashes when used with t3 instance generations as argument__ 

### evironment setup
```
$ scp -i <ssh credentials> aws/install-hm-host target@hostname:install-hm-host
$ ssh -i <ssh credentials> target@hostname
host$ chmod +x install-hm-host
host$ . ./install-hm-host
```
__Please note the former dot at install-hm-host invocation__

Stay put. The script should ask for confirmations such as ssh passphrase, erlang configure options, erlang make options... .

At this stage you should have all dependencies required by hmb2 installed.

### deployment

After the environement setup successfully, you must use the __hm__ deployment package in order to setup and launch hmb2.

__hm__ is a simple bash script with multiple options which invoke in turn administrative tools. See `host$ ./hm help`.

First : setup up the __mnesia__ table system

```
host$:hm-host cd hm-host
host$:hm-host ./hm boot-nodes
host$:hm-host ./hm boot_app oahm start
host$:hm-host ./hm attach oahm
```

You are now in the __oahm__ console from where you must setup the __mnesia__ table system

```
> oahm:isys_setup().
  ...
> all_tables_created
```

You can optionally populate the database with random users

```
> oahm:stuff_isys({Type,N}).
> mnesia_stuffed
```

_Type_ = uc | lw

  * __uc__ stands for _user chained_. i.e. each user is linked with the next one. 
  * __lw__ stands for _little world_. i.e. each user get a set of contacts and adds statistically in order to mimic a social network aggregation.
 
_N_ is the user number.

All these operations can be performed at once 

```
> oahm:isys_setup({Type,N}).
  ...
> all_in_one
```

Once your done, it is time to stop the nodes and boot all releases over the table system you just setup.

```
> oahm:stop_host().
> Ctrl-D
```

Before you start the system, you must also setup an aws profile. __TODO__ move in aws/install-hm-host

```
host$ ./install-iam
```

You can now launch the hmb2 unit.

```
host$ ./hm boot_host
```

### configuration
Prior to deployement you can check at the applications configuration file. To do so, edit `confihm.conf` (See confihm README).

### current versions

Because hmb2 uses __relflow__ plugin, we use an internal version number called `hm\_vsn`. It is implemented
as a environement variable in `api`, `data`, `logic`, `trans` and `oahm`applications. The static library `blihm` uses the 
regular `vsn` property for OTP applications.

  * __blihm__ 0.2.1
  * __api__ 0.2.0
  * __data__ 0.2.0
  * __logic__ 0.2.0
  * __trans__ 0.2.0
  * __oahm__ 0.2.0
